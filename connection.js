var mysql = require('mysql');

//make database connection
const conn = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'testOrenda'
});

conn.connect((err) => {
    if(err) throw err;
    console.log('MySQL Connected');
});

module.exports = conn;