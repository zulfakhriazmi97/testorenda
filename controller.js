'use strict';

var response = require('./res');
var connection = require('./connection');
const conn = require('./connection');

exports.index = function (req, res) {
    response.ok("Aplikasi Berjalan", res)
};

exports.tampilData = function (req, res) {
    connection.query("SELECT * FROM mahasiswa", function (error, rows, fields) {
        if (error) {
            console.log(error);
        } else {
            response.ok(rows, res);
        }
    });
};

//tampil berdasarkan id
exports.byid = function (req, res) {
    let id = req.params.id
    connection.query("SELECT * FROM mahasiswa where id = ?", [id], function (error, rows, fields) {
        if (error) {
            console.log(error);
        } else {
            response.ok(rows, res);
        }
    });
}

//tambah data
exports.tambahdata = function (req, res) {
    var nim     = req.body.nim;
    console.log('nim >', nim);
    var nama    = req.body.nama;
    var jurusan = req.body.jurusan;
    
    connection.query("INSERT INTO mahasiswa (nim,nama,jurusan) VALUES(?,?,?)", 
        [nim,nama,jurusan],
        function(error, rows, fields) {
            if(error) {
                console.log(error);
            }
            else {
                response.ok("Berhasil menambahkan data", res);
            }
        }
    );
};
