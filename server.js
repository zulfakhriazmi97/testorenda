const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//call route
var route = require('./routes');
route(app);

app.listen(3000, () => {
    console.log('server started on port 3000');
});

